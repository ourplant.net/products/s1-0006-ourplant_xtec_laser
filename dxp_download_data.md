Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s1-0006-ourplant_xtec_laser).

| document | download options |
|:-------- | ----------------:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s1-0006-ourplant_xtec_laser/-/raw/main/01_operating_manual/S1-0006_D_BA_OurPlant%20XTec%20Laser.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s1-0006-ourplant_xtec_laser/-/raw/main/02_assembly_drawing/s1-0006-C_ZNB_ourplant_laser.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s1-0006-ourplant_xtec_laser/-/raw/main/03_circuit_diagram/S1_0005_0006_OurPlant_XTec-_Laser-R.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s1-0006-ourplant_xtec_laser/-/raw/main/04_maintenance_instructions/S1-0006_A_WA_OurPlant%20XTec%20Laser.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s1-0006-ourplant_xtec_laser/-/raw/main/05_spare_parts/S1-0006_C_EVL_OurPlant%20XTec%20Laser.pdf), [en](https://gitlab.com/ourplant.net/products/s1-0006-ourplant_xtec_laser/-/raw/main/05_spare_parts/S1-0006_A_SWP_OurPlant%20XTec%20Laser.pdf)|

